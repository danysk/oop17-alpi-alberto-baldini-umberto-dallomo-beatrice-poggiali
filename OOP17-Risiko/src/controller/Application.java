package controller;

/**
 * 
 * Main class, just calls the starting menu.
 */
public final class Application {

    private Application() { }

/**
 * 
 * @param args not used.
 */
    public static void main(final String[] args) {

           view.mainmenu.MenuIni.startMenu();
    }
}
