# README #

##Description:

This program is a free software, you can redistribute and/or modify it. It was developed for the course of Object-Oriented Programming, faculty of Engineering and Computer Science, University of Bologna, Italy.

##Usage instructions:

1) Download __Risiko.jar__ file  
2) Run it

###Developers:

* Umberto Baldini(IT)
* Beatrice Dall'Omo(IT)
* Alberto Alpi(IT)
* Poggiali Francesco(IT)